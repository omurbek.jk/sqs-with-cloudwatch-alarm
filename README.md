

## Description


## Hierarchy



## Required variables with no default

### `ALARM_EMAIL`:
  * description: EMail address to notify if there are any operational issues


## Required variables with default

### `STACK_NAME`:
  * description: The name of the Cloudformation stack that this template will create
  * default: test-stack

### `DISABLE_ROLLBACK`:
  * description: Set to true to disable rollback of the stack if stack creation failed
  * default: false

### `ON_FAILURE`:
  * description: Determines what action will be taken if stack creation fails. This must be one of: DO_NOTHING, ROLLBACK, or DELETE. You can specify either OnFailure or DisableRollback, but not both.
  * default: ROLLBACK

### `TIMEOUT_IN_MINUTES`:
  * description: The amount of time that can pass before the stack status becomes CREATE_FAILEDThe amount of time that can pass before the stack status becomes CREATE_FAILED; if DISABLE_ROLLBACK is not set or is set to false, the stack will be rolled back.
  * default: 60

### `CAPABILTIES`:
  * description: A list of values that you must specify before AWS CloudFormation can create certain stacks. Some stack templates might include resources that can affect permissions in your AWS account, for example, by creating new AWS Identity and Access Management (IAM) users. For those stacks, you must explicitly acknowledge their capabilities by specifying this parameter.
  * default: CAPABILITY_IAM, CAPABILITY_NAMED_IAM


## Optional variables with default

**None**


## Optional variables with no default

**None**

## Tags


## Categories


## Diagram


## Icon


